package ke.co.healthix.healthixbmi.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;
import ke.co.healthix.healthixbmi.R;
import ke.co.healthix.healthixbmi.common.Commons;
import ke.co.healthix.healthixbmi.data.DataManager;
import ke.co.healthix.healthixbmi.data.Keys;
import ke.co.healthix.healthixbmi.dialog.ProgressDialog;
import ke.co.healthix.healthixbmi.internet.Internet;
import ke.co.healthix.healthixbmi.internet.URLs;

/**
 * Created by ojiambo on 12/04/2017.
 * Healthix Solutions copyright 2017.
 */

public class LogInActivity extends AppCompatActivity {

    private RelativeLayout llLogIn;
    private TextInputLayout tilUsername;
    private EditText etUsername;
    private TextInputLayout tilPassword;
    private EditText etPassword;
    private Button btnLogIn;

    private Bundle data;
    private DataManager manager;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log_in);
        setData();
        setUI();
        setFunctionality();
    }

    private void setData(){
        data = new Bundle();
        manager = new DataManager(LogInActivity.this);
    }

    private void setUI(){
        llLogIn = (RelativeLayout) findViewById(R.id.rl_log_in);
        tilUsername = (TextInputLayout) findViewById(R.id.til_username);
        etUsername = (EditText) findViewById(R.id.et_username);
        tilPassword = (TextInputLayout) findViewById(R.id.til_password);
        etPassword = (EditText) findViewById(R.id.et_password);
        btnLogIn = (Button) findViewById(R.id.btn_log_in);
    }

    private void setFunctionality(){
        btnLogIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logIn();
            }
        });
    }

    private void logIn(){
        tilUsername.setError(null);
        tilPassword.setError(null);

        String username = etUsername.getText().toString();
        String password = etPassword.getText().toString();

        if(username.length() == 0){
            tilUsername.setError(getString(R.string.msg_username_required));
        }else if(password.length() == 0){
            tilPassword.setError(getString(R.string.msg_password_required));
        }else if(!Commons.isValidEmail(username)){
            tilUsername.setError(getString(R.string.msg_username_invalid));
        }else{
            logIn(username, password);
        }
    }

    private void logIn(String username, String password){
        RequestParams params = new RequestParams();
        params.put("username", username);
        params.put("password", password);

        final AsyncHttpClient client = new AsyncHttpClient();
        client.post(URLs.URL_LOG_IN, params, new JsonHttpResponseHandler(){

            private ProgressDialog dialog;
            private Snackbar snackbar;

            @Override
            public void onStart() {
                super.onStart();
                if(!Internet.isNetworkAvailable(LogInActivity.this)){
                    client.cancelAllRequests(true);
                    snackbar = Snackbar.make(llLogIn, R.string.msg_no_internet, Snackbar.LENGTH_INDEFINITE);
                    snackbar.setAction(R.string.act_settings, new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Commons.openSettings(LogInActivity.this);
                        }
                    });
                    snackbar.show();
                }else {
                    dialog = new ProgressDialog(LogInActivity.this);
                    dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                        @Override
                        public void onCancel(DialogInterface dialog) {
                            client.cancelAllRequests(true);
                        }
                    });
                    dialog.show();
                }
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                try{
                    if(response.getInt("success") == 1){
                        manager.setCurrentUser(response.toString());
                        Intent intent = new Intent(LogInActivity.this, HomeActivity.class);
                        intent.putExtra(Keys.EXTRA_BUNDLE, data);
                        startActivity(intent);
                        LogInActivity.this.finish();
                    }else if(response.getInt("success") == 0) {
                        Snackbar.make(llLogIn, response.getString("Error"), Snackbar.LENGTH_LONG).show();
                    }else {
                        throw new Exception();
                    }
                }catch (Exception e){
                    Snackbar.make(llLogIn, R.string.msg_an_error_occurred, Snackbar.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                try{
                    Snackbar.make(llLogIn, errorResponse.getString("Error"), Snackbar.LENGTH_LONG).show();
                }catch (Exception e){
                    Snackbar.make(llLogIn, R.string.msg_an_error_occurred, Snackbar.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                super.onFailure(statusCode, headers, responseString, throwable);
                Snackbar.make(llLogIn, R.string.msg_an_error_occurred, Snackbar.LENGTH_LONG).show();
            }

            @Override
            public void onFinish() {
                super.onFinish();
                if(dialog != null && dialog.isShowing()){
                    dialog.dismiss();
                }
            }
        });
    }

}
