package ke.co.healthix.healthixbmi.data.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Eugene Ojiambo on 12/04/2017.
 * Healthix Solutions copyright 2017.
 */

public class Record implements Parcelable {

    private String id;
    private String name;
    private double weight;
    private double height;

    public Record() {

    }

    public void setId(String id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public double getWeight() {
        return weight;
    }

    public double getHeight() {
        return height;
    }

    private Record(Parcel in){
        id = in.readString();
        name = in.readString();
        weight = in.readDouble();
        height = in.readDouble();
    }

    public static final Creator CREATOR = new Creator() {
        public Record createFromParcel(Parcel in) {
            return new Record(in);
        }
        public Record[] newArray(int size) {
            return new Record[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(name);
        dest.writeDouble(weight);
        dest.writeDouble(height);
    }

}
