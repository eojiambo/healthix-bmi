package ke.co.healthix.healthixbmi.data.storage;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import ke.co.healthix.healthixbmi.R;
import ke.co.healthix.healthixbmi.data.model.Record;

/**
 * Created by Eugene Ojiambo on 12/04/2017.
 * Healthix Solutions copyright 2017.
 */

public class StorageManager {

    private Context context;
    private SQLiteDatabase db;
    private Cursor cursor;

    private static final String DB_HEALTHIX_BMI = "db_healthix_bmi";
    private static final String TABLE_BMI_RECORDS = "bmi_records";
    private static final String COLUMN_ID = "id";
    private static final String COLUMN_NAME = "name";
    private static final String COLUMN_WEIGHT = "weight";
    private static final String COLUMN_HEIGHT = "height";

    public StorageManager(Context context) {
        this.context = context;
    }

    private boolean openOrCreateDatabase() throws Exception {
        db = context.openOrCreateDatabase(DB_HEALTHIX_BMI, SQLiteDatabase.CREATE_IF_NECESSARY, null);
        db.execSQL("CREATE TABLE IF NOT EXISTS `" + TABLE_BMI_RECORDS + "` ("
                + COLUMN_ID + " INTEGER primary key AUTOINCREMENT,"
                + COLUMN_NAME + " VARCHAR,"
                + COLUMN_WEIGHT + " DOUBLE,"
                + COLUMN_HEIGHT + " DOUBLE);");
        return true;
    }

    public void createRecord(Record record){
        try {
            if(openOrCreateDatabase()){
                String sql = "INSERT INTO `" + TABLE_BMI_RECORDS + "` VALUES(NULL, '" + record.getName() + "', " + record.getWeight() + ", " + record.getHeight() + ");";
                db.execSQL(sql);
            }else{
                throw new Exception();
            }
        }catch (Exception e){
            Toast.makeText(context, R.string.msg_an_error_occurred, Toast.LENGTH_SHORT).show();
        }
    }

    public List<Record> retrieveRecords(){
        try {
            if(openOrCreateDatabase()){
                List<Record> records = new ArrayList<>();
                String sql = "SELECT * FROM `" + TABLE_BMI_RECORDS + "` ORDER BY `" + COLUMN_ID + "` DESC";
                cursor = db.rawQuery(sql, null);
                if(cursor.getCount() > 0){
                    cursor.moveToFirst();
                    do {
                        Record record = new Record();
                        record.setId(cursor.getString(cursor.getColumnIndex(COLUMN_ID)));
                        record.setName(cursor.getString(cursor.getColumnIndex(COLUMN_NAME)));
                        record.setWeight(cursor.getDouble(cursor.getColumnIndex(COLUMN_WEIGHT)));
                        record.setHeight(cursor.getDouble(cursor.getColumnIndex(COLUMN_HEIGHT)));
                        records.add(record);
                    } while (cursor.moveToNext());
                }
                return records;
            } else {
                throw new Exception();
            }
        } catch (Exception e) { e.printStackTrace();
            Toast.makeText(context, R.string.msg_an_error_occurred, Toast.LENGTH_SHORT).show();
            return new ArrayList<>();
        }
    }

}
