package ke.co.healthix.healthixbmi.dialog;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatDialog;

import ke.co.healthix.healthixbmi.R;

/**
 * Created by Eugene Ojiambo on 12/04/2017.
 * Healthix Solutions copyright copyright 2017.
 */

public class ProgressDialog extends AppCompatDialog {

    public ProgressDialog(Context context) {
        super(context);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_progress);
        setData();
        setUI();
        setFunctionality();
    }

    private void setData() {

    }

    private void setUI() {

    }

    private void setFunctionality() {

    }

}
