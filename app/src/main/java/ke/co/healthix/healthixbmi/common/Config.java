package ke.co.healthix.healthixbmi.common;

/**
 * Created by Eugene Ojiambo on 12/04/2017.
 * Healthix Solutions copyright 2017.
 */

public class Config {

    public static final String REMARK_UNDERWEIGHT = "UNDERWEIGHT";
    public static final String REMARK_HEALTHY = "HEALTHY";
    public static final String REMARK_OVERWEIGHT = "OVERWEIGHT";
    public static final String REMARK_OBESE = "OBESE";

    public Config() {

    }

}
