package ke.co.healthix.healthixbmi.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import java.util.List;

import ke.co.healthix.healthixbmi.R;
import ke.co.healthix.healthixbmi.data.DataManager;
import ke.co.healthix.healthixbmi.data.Keys;
import ke.co.healthix.healthixbmi.data.model.Record;
import ke.co.healthix.healthixbmi.fragment.MyAccountDialogFragment;
import ke.co.healthix.healthixbmi.userinterface.recycleradpater.HomeRecyclerAdapter;

/**
 * Created by ojiambo on 12/04/2017.
 * Healthix Solutions copyright 2017.
 */

public class HomeActivity extends AppCompatActivity {

    private CollapsingToolbarLayout ctlHome;
    private Toolbar tbHome;
    private TextView tvUnderWeight;
    private TextView tvHealthy;
    private TextView tvOverweight;
    private TextView tvMessage;
    private RecyclerView rvHome;
    private FloatingActionButton fabHome;

    private Bundle data;
    private DataManager manager;
    private List<Record> records;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        setData();
        setUI();
        setFunctionality();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.act_my_account:
                myAccount();
                return true;
            case R.id.act_log_out:
                logOut();
                return true;
            default:
                return false;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        setData();
        setHome();
    }

    private void setData(){
        data = new Bundle();
        manager = new DataManager(HomeActivity.this);
        records = manager.retrieveRecords();
    }

    private void setUI(){
        ctlHome = (CollapsingToolbarLayout) findViewById(R.id.ctl_home);
        tbHome = (Toolbar) findViewById(R.id.tb_home);
        tvUnderWeight = (TextView) findViewById(R.id.tv_underweight);
        tvHealthy = (TextView) findViewById(R.id.tv_healthy);
        tvOverweight = (TextView) findViewById(R.id.tv_overweight);
        tvMessage = (TextView) findViewById(R.id.tv_message);
        rvHome = (RecyclerView) findViewById(R.id.rv_home);
        fabHome = (FloatingActionButton) findViewById(R.id.fab_home);

        ctlHome.setExpandedTitleColor(ContextCompat.getColor(HomeActivity.this, android.R.color.transparent));
        setSupportActionBar(tbHome);
    }

    private void setFunctionality(){
        fabHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomeActivity.this, NewRecordActivity.class);
                intent.putExtra(Keys.EXTRA_BUNDLE, data);
                startActivity(intent);
            }
        });
    }

    private void setHome(){
        tvUnderWeight.setText(String.valueOf(manager.getRecordsGroupCount(getString(R.string.tv_underweight))));
        tvHealthy.setText(String.valueOf(manager.getRecordsGroupCount(getString(R.string.tv_healthy))));
        tvOverweight.setText(String.valueOf(manager.getRecordsGroupCount(getString(R.string.tv_overweight))));
        rvHome.setLayoutManager(new LinearLayoutManager(HomeActivity.this));
        rvHome.setNestedScrollingEnabled(false);
        rvHome.setAdapter(new HomeRecyclerAdapter(HomeActivity.this, records));
        if(records.size() > 0){
            tvMessage.setVisibility(TextView.GONE);
        }
    }

    private void myAccount(){
        MyAccountDialogFragment fragment = new MyAccountDialogFragment();
        fragment.show(getSupportFragmentManager(), "My account");
    }

    private void logOut(){
        manager.setCurrentUser(null);
        Intent intent = new Intent(HomeActivity.this, LogInActivity.class);
        startActivity(intent);
        HomeActivity.this.finish();
    }

}
