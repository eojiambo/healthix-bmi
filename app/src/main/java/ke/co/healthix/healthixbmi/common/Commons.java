package ke.co.healthix.healthixbmi.common;

import android.content.Context;
import android.content.Intent;
import android.provider.Settings;

import java.math.RoundingMode;
import java.text.DecimalFormat;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

/**
 * Created by Eugene Ojiambo on 12/04/2017.
 * Healthix Solutions copyright 2017.
 */

public class Commons {

    public Commons() {

    }

    public static boolean isValidEmail(String email){
        try {
            InternetAddress address = new InternetAddress(email);
            address.validate();
            return true;
        } catch (AddressException e) {
            return false;
        }
    }

    public static void openSettings(Context context){
        Intent intent = new Intent(Settings.ACTION_SETTINGS);
        context.startActivity(intent);
    }

    public static double getBMI(double weight, double height){
        return round2DP(weight / Math.pow(height, 2));
    }

    private static double round2DP(double num){
        DecimalFormat df = new DecimalFormat("#.##");
        df.setRoundingMode(RoundingMode.HALF_UP);
        return Double.parseDouble(df.format(num));
    }

    public static String getRemark(double bmi){
        if(bmi < 18.5){
            return Config.REMARK_UNDERWEIGHT;
        }else if(bmi >= 18.5 && bmi <= 25){
            return Config.REMARK_HEALTHY;
        }else if (bmi > 25 && bmi <= 30){
            return Config.REMARK_OVERWEIGHT;
        }else{
            return Config.REMARK_OBESE;
        }
    }

}
