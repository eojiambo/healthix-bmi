package ke.co.healthix.healthixbmi.internet;

/**
 * Created by Eugene Ojiambo on 12/04/2017.
 * Healthix Solutions copyright 2017.
 */

public class URLs {

    private static final String URL_BASE = "http://onboarding.healthixsolutions.com/api/v1/";
    public static final String URL_LOG_IN = URL_BASE + "user/userlogin/";

    public URLs() {

    }

}
