package ke.co.healthix.healthixbmi.data;

import android.content.Context;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import ke.co.healthix.healthixbmi.common.Commons;
import ke.co.healthix.healthixbmi.data.model.Record;
import ke.co.healthix.healthixbmi.data.model.User;
import ke.co.healthix.healthixbmi.data.storage.PreferenceManager;
import ke.co.healthix.healthixbmi.data.storage.StorageManager;

/**
 * Created by Eugene Ojiambo on 12/04/2017.
 * Healthix Solutions copyright 2017.
 */

public class DataManager {

    private Context context;
    private StorageManager manager;

    public DataManager(Context context) {
        this.context = context;
    }

    public void setCurrentUser(String userData){
        PreferenceManager.setStringPreference(context, Keys.PREF_CURRENT_USER, userData);
    }

    public User getCurrentUser() throws Exception {
        JSONObject userData = new JSONObject(PreferenceManager.getStringPreference(context, Keys.PREF_CURRENT_USER, null));
        User user = new User();
        user.setKey(userData.getString("key"));
        user.setId(userData.getString("id"));
        user.setName(userData.getString("name"));
        user.setEmail(userData.getString("email"));
        user.setCompany(userData.getString("company"));
        user.setRoles(getRoles(userData.getJSONArray("roles")));
        return user;
    }

    private List<String> getRoles(JSONArray rolesData) throws Exception {
        List<String> roles = new ArrayList<>();
        for(int i = 0; i < rolesData.length(); i++){
            rolesData.getString(i);
        }
        return roles;
    }

    public void createRecord(Record record){
        manager = new StorageManager(context);
        manager.createRecord(record);
    }

    public List<Record> retrieveRecords(){
        manager = new StorageManager(context);
        return manager.retrieveRecords();
    }

    public int getRecordsGroupCount(String remark){
        int count = 0;
        List<Record> records = retrieveRecords();
        for(Record record : records){
            if(Commons.getRemark(Commons.getBMI(record.getWeight(), record.getHeight())).equalsIgnoreCase(remark)){
                ++count;
            }
        }
        return count;
    }

}
