package ke.co.healthix.healthixbmi.data.storage;

import android.content.Context;
import android.content.SharedPreferences;

import ke.co.healthix.healthixbmi.data.Keys;

/**
 * Created by Eugene Ojiambo on 12/04/2017.
 * Healthix Solutions copyright copyright 2017.
 */

public class PreferenceManager {

    public PreferenceManager(){

    }

    public static void setStringPreference(Context context, String key, String value){
        SharedPreferences preferences = context.getSharedPreferences(Keys.PREFS_HEALTHIX_BMI, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(key, value);
        editor.apply();
    }

    public static String getStringPreference(Context context, String key, String defaultValue){
        SharedPreferences preferences = context.getSharedPreferences(Keys.PREFS_HEALTHIX_BMI, Context.MODE_PRIVATE);
        return preferences.getString(key, defaultValue);
    }

}
