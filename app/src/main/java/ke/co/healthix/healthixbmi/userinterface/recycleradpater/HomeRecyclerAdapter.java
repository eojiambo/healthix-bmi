package ke.co.healthix.healthixbmi.userinterface.recycleradpater;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;

import ke.co.healthix.healthixbmi.R;
import ke.co.healthix.healthixbmi.common.Commons;
import ke.co.healthix.healthixbmi.common.Config;
import ke.co.healthix.healthixbmi.data.model.Record;

/**
 * Created by Eugene Ojiambo on 12/04/2017.
 * Healthix Solutions copyright 2017.
 */

public class HomeRecyclerAdapter extends RecyclerView.Adapter<HomeRecyclerAdapter.ViewHolder> {

    private Context context;
    private List<Record> records;

    public HomeRecyclerAdapter(Context context, List<Record> records){
        this.context = context;
        this.records = records;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.recycler_adapter_home, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final Record record = records.get(position);
        double bmi = Commons.getBMI(record.getWeight(), record.getHeight());
        holder.tvName.setText(record.getName());
        holder.tvBMI.setText(String.valueOf(bmi));
        holder.tvWeightHeight.setText(String.valueOf(record.getWeight())
                .concat(context.getString(R.string.tv_kg))
                .concat("/")
                .concat(String.valueOf(record.getHeight()))
                .concat(context.getString(R.string.tv_m))
        );
        String remark = Commons.getRemark(bmi);
        holder.tvMarker.setText(remark);
        switch (remark){
            case Config.REMARK_UNDERWEIGHT:
                holder.tvMarker.setBackgroundResource(R.drawable.shp_marker_underweight);
                break;
            case Config.REMARK_HEALTHY:
                holder.tvMarker.setBackgroundResource(R.drawable.shp_marker_healthy);
                break;
            case Config.REMARK_OVERWEIGHT:
                holder.tvMarker.setBackgroundResource(R.drawable.shp_marker_overweight);
                break;
            default:
                holder.tvMarker.setBackgroundResource(R.drawable.shp_marker_healthy);
                break;
        }
        if(position == records.size() - 1){
            holder.vHome.setVisibility(View.INVISIBLE);
        }

        holder.rlHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }

    @Override
    public int getItemCount() {
        return records.size();
    }

    protected class ViewHolder extends RecyclerView.ViewHolder {

        private RelativeLayout rlHome;
        private TextView tvName;
        private TextView tvBMI;
        private TextView tvWeightHeight;
        private TextView tvMarker;
        private View vHome;

        public ViewHolder(View itemView) {
            super(itemView);
            rlHome = (RelativeLayout) itemView.findViewById(R.id.rl_home);
            tvName = (TextView) itemView.findViewById(R.id.tv_name);
            tvBMI = (TextView) itemView.findViewById(R.id.tv_bmi);
            tvWeightHeight = (TextView) itemView.findViewById(R.id.tv_weight_height);
            tvMarker = (TextView) itemView.findViewById(R.id.tv_marker);
            vHome = itemView.findViewById(R.id.v_home);
        }
    }

}
