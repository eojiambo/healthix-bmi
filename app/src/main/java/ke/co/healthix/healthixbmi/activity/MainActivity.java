package ke.co.healthix.healthixbmi.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import ke.co.healthix.healthixbmi.data.DataManager;

/**
 * Created by Eugene Ojiambo on 12/04/2017.
 * Healthix Solutions copyright 2017.
 */

public class MainActivity extends AppCompatActivity {

    private DataManager manager;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setData();
        setUI();
        setFunctionality();
    }

    private void setData(){
        manager = new DataManager(MainActivity.this);
    }

    private void setUI(){
        Intent intent = null;
        try {
            manager.getCurrentUser();
            intent = new Intent(MainActivity.this, HomeActivity.class);
        }catch (Exception e){
            intent = new Intent(MainActivity.this, LogInActivity.class);
        }finally {
            startActivity(intent);
            MainActivity.this.finish();
        }
    }

    private void setFunctionality(){

    }

}
