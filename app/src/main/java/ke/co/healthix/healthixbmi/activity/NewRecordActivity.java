package ke.co.healthix.healthixbmi.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;

import ke.co.healthix.healthixbmi.R;
import ke.co.healthix.healthixbmi.data.DataManager;
import ke.co.healthix.healthixbmi.data.Keys;
import ke.co.healthix.healthixbmi.data.model.Record;

/**
 * Created by ojiambo on 12/04/2017.
 * Healthix Solutions copyright 2017.
 */

public class NewRecordActivity extends AppCompatActivity {

    private Toolbar tbNewRecord;
    private TextInputLayout tilName;
    private EditText etName;
    private TextInputLayout tilWeight;
    private EditText etWeight;
    private TextInputLayout tilHeight;
    private EditText etHeight;

    private Bundle data;
    private DataManager manager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_record);
        setData();
        setUI();
        setFunctionality();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_new_record, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                onBackPressed();
                return true;
            case R.id.act_save:
                save();
                return true;
            default:
                return false;
        }
    }

    private void setData(){
        data = getIntent().getBundleExtra(Keys.EXTRA_BUNDLE);
        manager = new DataManager(NewRecordActivity.this);
    }

    private void setUI(){
        tbNewRecord = (Toolbar) findViewById(R.id.tb_new_record);
        tilName = (TextInputLayout) findViewById(R.id.til_name);
        etName = (EditText) findViewById(R.id.et_name);
        tilWeight = (TextInputLayout) findViewById(R.id.til_weight);
        etWeight = (EditText) findViewById(R.id.et_weight);
        tilHeight = (TextInputLayout) findViewById(R.id.til_height);
        etHeight = (EditText) findViewById(R.id.et_height);

        setSupportActionBar(tbNewRecord);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void setFunctionality(){

    }

    private void save(){
        tilName.setError(null);
        tilWeight.setError(null);
        tilHeight.setError(null);

        String name = etName.getText().toString().trim();
        String weight = etWeight.getText().toString().trim();
        String height = etHeight.getText().toString().trim();

        if(name.length() == 0){
            tilName.setError(getString(R.string.msg_name_required));
        }else if(name.length() < 2){
            tilName.setError(getString(R.string.msg_name_short));
        }else if(weight.length() == 0){
            tilWeight.setError(getString(R.string.msg_weight_required));
        }else if(height.length() == 0){
            tilHeight.setError(getString(R.string.msg_height_required));
        }else{
            Record record = new Record();
            record.setName(name);
            record.setWeight(Double.parseDouble(weight));
            record.setHeight(Double.parseDouble(height));
            manager.createRecord(record);
            NewRecordActivity.this.finish();
        }
    }

}
