package ke.co.healthix.healthixbmi.data.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Eugene Ojiambo on 12/04/2017.
 * Healthix Solutions copyright 2017.
 */

public class User implements Parcelable {

    private String key;
    private String id;
    private String name;
    private String email;
    private String company;
    private List<String> roles;

    public User() {

    }

    public void setKey(String key) {
        this.key = key;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public void setRoles(List<String> roles) {
        this.roles = roles;
    }

    public String getKey() {
        return key;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public String getCompany() {
        return company;
    }

    public List<String> getRoles() {
        return roles;
    }

    private User(Parcel in){
        key = in.readString();
        id = in.readString();
        name = in.readString();
        email = in.readString();
        company = in.readString();
        roles = new ArrayList<>();
        in.readList(roles, String.class.getClassLoader());
    }

    public static final Creator CREATOR = new Creator() {
        public User createFromParcel(Parcel in) {
            return new User(in);
        }
        public User[] newArray(int size) {
            return new User[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(key);
        dest.writeString(id);
        dest.writeString(name);
        dest.writeString(email);
        dest.writeString(company);
        dest.writeList(roles);
    }

}
