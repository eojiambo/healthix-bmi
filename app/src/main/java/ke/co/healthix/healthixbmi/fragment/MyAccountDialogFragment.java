package ke.co.healthix.healthixbmi.fragment;

import android.app.Dialog;
import android.support.design.widget.BottomSheetDialogFragment;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import ke.co.healthix.healthixbmi.R;
import ke.co.healthix.healthixbmi.data.DataManager;
import ke.co.healthix.healthixbmi.data.model.User;

/**
 * Created by Eugene Ojiambo on 12/04/2017.
 * Healthix Solutions copyright 2017.
 */

public class MyAccountDialogFragment extends BottomSheetDialogFragment {

    private View rootView;
    private TextView tvName;
    private TextView tvEmail;
    private TextView tvCompany;
    private TextView tvRoles;
    
    private DataManager manager;
    private User user;

    @Override
    public void setupDialog(Dialog dialog, int style) {
        super.setupDialog(dialog, style);
        
        manager = new DataManager(getContext());
        try {
            user =  manager.getCurrentUser();
        } catch (Exception e) {
            dismiss();
            Toast.makeText(getContext(), R.string.msg_user_details_unavailable, Toast.LENGTH_SHORT).show();
            return;
        }

        rootView = View.inflate(getContext(), R.layout.fragment_my_account, null);
        dialog.setContentView(rootView);

        tvName = (TextView) rootView.findViewById(R.id.tv_name);
        tvEmail = (TextView) rootView.findViewById(R.id.tv_email);
        tvCompany = (TextView) rootView.findViewById(R.id.tv_company);
        tvRoles = (TextView) rootView.findViewById(R.id.tv_roles);

        tvName.setText(user.getName());
        tvEmail.setText(user.getEmail());
        tvCompany.setText(user.getCompany());

        List<String> roles = user.getRoles();
        for(int i = 0; i < roles.size(); i++){
            if(i == 0){
                tvRoles.setText(roles.get(i));
            }else{
                tvRoles.setText(tvRoles.getText().toString().concat(", ").concat(roles.get(i)));
            }
        }
    }
}
