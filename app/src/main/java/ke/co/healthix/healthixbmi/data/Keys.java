package ke.co.healthix.healthixbmi.data;

/**
 * Created by Eugene Ojiambo on 12/04/2017.
 * Healthix Solutions copyright 2017.
 */

public class Keys {

    public static final String EXTRA_BUNDLE = "extra_bundle";

    public static final String PREFS_HEALTHIX_BMI = "prefs_healthix_bmi";
    public static final String PREF_CURRENT_USER = "pref_current_user";

    public Keys() {

    }

}
